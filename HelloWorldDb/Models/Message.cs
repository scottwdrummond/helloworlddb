﻿namespace HelloWorldDb.Models
{
    public partial class Message
    {
        public long Id { get; set; }
        public string Text { get; set; }
    }
}

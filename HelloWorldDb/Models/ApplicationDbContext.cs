﻿using Microsoft.EntityFrameworkCore;

namespace HelloWorldDb.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        { }

        public virtual DbSet<Message> Message { get; set; }


    }
}
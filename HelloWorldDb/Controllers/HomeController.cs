﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using HelloWorldDb.Models;
using System.Linq;
using System;

namespace HelloWorldDb.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext _context { get; set; }

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            Message message;
            try
            {
                message = _context.Set<Message>().FirstOrDefault();
            }
            catch (Exception e)
            {
                message = new Message { Text = $"An Exception occurred. {e.Message}" };
            }
            return View(message);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
